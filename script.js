/*
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо. */


let input1 = document.querySelector(".input1");
let input2 = document.querySelector(".input2");
let btn =   document.querySelector(".btn");

function toggle() {
    let toggle = document.querySelectorAll('.fas');

    toggle.forEach(el => el.addEventListener('click', function () {
        if (el.classList.contains('fa-eye')) {
            el.classList.remove('fa-eye');
            el.classList.add('fa-eye-slash');
            el.previousElementSibling.type = 'text';
        } else {
            el.classList.remove('fa-eye-slash');
            el.classList.add('fa-eye');
            el.previousElementSibling.type = 'password';
        }
    }))


    document.querySelector(".btn").addEventListener("click", function () {
        if (input1.value === input2.value) {
            alert("You are welcome");
        } else {
            btn.insertAdjacentHTML("afterend", '<span class = "red">Потрібно ввести однакові значення</span>')
                }
    })

    
}

toggle()

